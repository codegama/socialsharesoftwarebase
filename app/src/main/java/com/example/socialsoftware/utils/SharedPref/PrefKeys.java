package com.example.socialsoftware.utils.SharedPref;

public class PrefKeys {

    private PrefKeys() {
    }

    public static final String IS_LOGGED_IN = "isLoggedIn";
    public static final String SESSION_TOKEN = "token";
    public static final String USER_ID = "id";
    public static final String USER_EMAIL = "email";
    public static final String USER_NAME = "userName";
    public static final String FIRST_NAME="firstname";
    public static final String LAST_NAME="lastname";
    public static final String USER_MOBILE = "moblie";
    public static final String USER_PICTURE ="picture" ;
    public static final String USER_ABOUT = "description";
    public static final String LOGIN_TYPE = "loginType";
    public static final String USER_GENDER = "gender";
    public static final String PUSH_NOTIFICATIONS = "push_notification_status";
    public static final String EMAIL_NOTIFICATIONS = "email_notification_status";
    public static final Object IS_SOCIAL_LOGIN = "isSocialLogin";
    public static final String USER_ACCOUNT_ID = "user_account_id";
    public static final String IS_SOCIAL_ACCOUNT_CONNECTED = "is_social_account_connected";

    public static final String POST_ID = "post_id";
    public static final String DESCRIPTION ="description";
    public static final String POST_PLATFORM ="platforms";
    public static final String TITLE = "title";
    public static final String STATUS ="status";
    public static final String PICTURE ="picture";
    public static final String ACCOUNT ="account";



}
