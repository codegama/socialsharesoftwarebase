package com.example.socialsoftware.network;


import android.util.Log;

import com.example.socialsoftware.network.APIConstants.ErrorCodes;
import  com.example.socialsoftware.network.APIConstants.Params;
import com.google.gson.JsonIOException;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class APIClient {

    public static Retrofit getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    Response response = chain.proceed(request);
                    ResponseBody responseBody = response.body();
                    BufferedSource source = responseBody.source();
                    source.request(Long.MAX_VALUE); // Buffer the entire body.
                    Buffer buffer = source.buffer();
                    String respData = buffer.clone().readString(Charset.defaultCharset());
                    Log.i("tag", response.toString());
                    JSONObject resp = null;
                    try {
                        resp = new JSONObject(respData);
                    }   catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (resp != null) {
                        switch (resp.optInt(APIConstants.Params.ERROR_CODE)) {
                            case ErrorCodes.TOKEN_EXPIRED:
                                emitEvent(ErrorCodes.TOKEN_EXPIRED, resp.optString(Params.ERROR));
                                break;

                            case ErrorCodes.USER_DOESNT_EXIST:
                                emitEvent(ErrorCodes.USER_DOESNT_EXIST, resp.optString(Params.ERROR));
                                break;

                            case ErrorCodes.USER_RECORD_DELETED_CONTACT_ADMIN:
                                emitEvent(ErrorCodes.USER_RECORD_DELETED_CONTACT_ADMIN, resp.optString(Params.ERROR));
                                break;

                            case ErrorCodes.INVALID_TOKEN:
                                emitEvent(ErrorCodes.INVALID_TOKEN, resp.optString(Params.ERROR));
                                break;

                            case ErrorCodes.USER_LOGIN_DECLINED:
                                emitEvent(ErrorCodes.USER_LOGIN_DECLINED, resp.optString(Params.ERROR));
                                break;

                            case ErrorCodes.EMAIL_NOT_ACTIVATED:
                                emitEvent(ErrorCodes.USER_LOGIN_DECLINED, resp.optString(Params.ERROR));
                                break;

                            case ErrorCodes.EMAIL_ALREADY_EXISTS:
                                emitEvent(ErrorCodes.EMAIL_ALREADY_EXISTS,resp.optString(Params.ERROR));
                                break;

                            case ErrorCodes.EMAIL_CONFIGURATION_FAILED:
                                emitEvent(ErrorCodes.EMAIL_CONFIGURATION_FAILED,resp.optString(Params.ERROR));
                                break;
                        }
                    }
                    return response;
                }).build();

        return new Retrofit.Builder()
                .baseUrl(APIConstants.URLs.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(client)
                .build();
    }

    private static void emitEvent(int code, String message) {
        EventBus.getDefault().post(new APIEvent(message, code));
    }

}
