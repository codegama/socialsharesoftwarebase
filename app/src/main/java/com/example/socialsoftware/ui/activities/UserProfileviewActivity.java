package com.example.socialsoftware.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileviewActivity extends BaseActivity {

    @BindView(R.id.userProfileimage)
    CircleImageView userProfileimage;
    @BindView(R.id.layoutname)
    View layoutname;
    @BindView(R.id.layoutFirstname)
    View layoutFirstname;
    @BindView(R.id.userfirstname)
    EditText userfirstname;
    @BindView(R.id.layoutlastname)
    View layoutlastname;
    @BindView(R.id.userEdit)
    ImageButton userEditbtn;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.userlastname)
    EditText userlastname;
    @BindView(R.id.userEmail)
    EditText userEmail;
    @BindView(R.id.userPhone)
    EditText userPhone;
    @BindView(R.id.userabout)
    EditText userabout;
    @BindView(R.id.saveupdates)
    Button saveupdates;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    APIInterface apiInterface;
    PrefUtils prefUtils;
    private Uri fileToUpload= null;
    private  boolean isEditMode;
    private static final int PICK_IMAGE = 100;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profileview);
        ButterKnife.bind(this);
        apiInterface= APIClient.getClient().create(APIInterface.class);

        toolbar.setTitle(getString(R.string.account));
        getSupportActionBar().hide();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getProfileDetails(null);

    }

    @Override
    public void onBackPressed(){
        if (!isEditMode) {
            super.onBackPressed();
        }
        else {
            layoutlastname.setVisibility(View.GONE);
            layoutFirstname.setVisibility(View.GONE);
            layoutname.setVisibility(View.VISIBLE);
            saveupdates.setVisibility(View.INVISIBLE);
            userEditbtn.setVisibility(View.VISIBLE);
            username.setEnabled(false);
            userEmail.setEnabled(false);
            userabout.setEnabled(false);
            userPhone.setEnabled(false);
            isEditMode=false;
        }
    }
    @OnClick({R.id.userProfileimage,R.id.saveupdates,R.id.userEdit})
    public void onViewClicked(View view){
        switch (view.getId()){
            case R.id.userProfileimage:
                if (isEditMode) {
                    callImagePicker();
                }
                updateProfileDetails(null);
                break;
            case R.id.saveupdates:
                getProfileDetails(fileToUpload);
                isEditMode=false;
                layoutlastname.setVisibility(View.GONE);
                layoutFirstname.setVisibility(View.GONE);
                layoutname.setVisibility(View.VISIBLE);
                saveupdates.setVisibility(View.INVISIBLE);
                userEditbtn.setVisibility(View.VISIBLE);
                username.setEnabled(false);
                userEmail.setEnabled(false);
                userabout.setEnabled(false);
                userPhone.setEnabled(false);
                break;
            case R.id.userEdit:
                isEditMode = true;
                layoutFirstname.setVisibility(View.VISIBLE);
                layoutlastname.setVisibility(View.VISIBLE);
                layoutname.setVisibility(View.INVISIBLE);
                userEditbtn.setVisibility(View.INVISIBLE);
                userEditbtn.setVisibility(View.GONE);
                saveupdates.setVisibility(View.VISIBLE);
                updateProfileDetails(null);

                userfirstname.setEnabled(isEditMode);
                userlastname.setEnabled(isEditMode);
                userEmail.setEnabled(isEditMode);
                userPhone.setEnabled(isEditMode);
                userabout.setEnabled(isEditMode);
                break;
        }
    }
    protected void getProfileDetails(Uri profileImageUri) {
        prefUtils = PrefUtils.getInstance(getApplicationContext());

        MultipartBody.Part multipartBody= null;

        Call<String> call;

       if (profileImageUri != null) {
            File file = new File(getRealPathFromURIPath(profileImageUri, this));

            // create RequestBody instance tempFrom file
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("images/*"), file);

            // MultipartBody.Part is used to send also the actual file name
            multipartBody =
                    MultipartBody.Part.createFormData(Params.PICTURE, file.getName(), requestFile);
           Log.i("Uri", String.valueOf(multipartBody));
        }

       UiUtils.showLoadingDialog(this);
       if (!isEditMode){
            saveupdates.setVisibility(View.INVISIBLE);
            call = apiInterface.getUserProfile(
                    prefUtils.getIntValue(PrefKeys.USER_ID, 0),
                    prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
       }
       else
          call = apiInterface.updateProfile(getPartFor(String.valueOf(prefUtils.getIntValue(PrefKeys.USER_ID, 0))),
                  getPartFor(prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, "")),
                  getPartFor(userfirstname.getText().toString()),
                  getPartFor(userlastname.getText().toString()),
                  getPartFor(userEmail.getText().toString()),
                  getPartFor(userPhone.getText().toString()),
                  getPartFor(userabout.getText().toString()),
                  multipartBody);
          call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                UiUtils.hideLoadingDialog();
                JSONObject profileResponse = null;
                if (response.isSuccessful()){
                    try {
                        profileResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (profileResponse != null) {
                        if (profileResponse.optString(APIConstants.Params.SUCCESS).equals(APIConstants.Constants.TRUE)) {
                            if (isEditMode)
                                UiUtils.showShortToast(UserProfileviewActivity.this, profileResponse.optString(APIConstants.Params.MESSAGE));
                                JSONObject data = profileResponse.optJSONObject(APIConstants.Params.DATA);
                                updateProfileDetails(data);
                                UiUtils.hideLoadingDialog();
                        } else {
                            UiUtils.showShortToast(UserProfileviewActivity.this, profileResponse.optString(Params.ERROR));
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(UserProfileviewActivity.this);
                UiUtils.showShortToast(UserProfileviewActivity.this,t.getMessage());
            }
        });
    }

    private void updateProfileDetails(JSONObject data) {
        if(data!=null){
            prefUtils.setValue(PrefKeys.USER_NAME,data.optString(Params.NAME));
            prefUtils.setValue(PrefKeys.FIRST_NAME, data.optString(Params.FIRST_NAME));
            prefUtils.setValue(PrefKeys.LAST_NAME, data.optString(Params.LAST_NAME));
            prefUtils.setValue(PrefKeys.USER_EMAIL, data.optString(Params.EMAIL));
            prefUtils.setValue(PrefKeys.USER_MOBILE, data.optString(Params.MOBILE));
            prefUtils.setValue(PrefKeys.USER_ABOUT, data.optString(Params.DESCRIPTION));
            prefUtils.setValue(PrefKeys.USER_PICTURE, data.optString(Params.PICTURE));

            //Set Values
            username.setText(prefUtils.getStringValue(PrefKeys.USER_NAME,""));
            userfirstname.setText(prefUtils.getStringValue(PrefKeys.FIRST_NAME,""));
            userlastname.setText(prefUtils.getStringValue(PrefKeys.LAST_NAME,""));
            userEmail.setText(prefUtils.getStringValue(PrefKeys.USER_EMAIL, ""));
            userPhone.setText(prefUtils.getStringValue(PrefKeys.USER_MOBILE,""));
            userabout.setText(prefUtils.getStringValue(PrefKeys.USER_ABOUT, ""));
            Glide.with(getApplicationContext()).load(prefUtils.getStringValue(PrefKeys.USER_PICTURE, "")).into(userProfileimage);

        }
        if (!isEditMode){

            userEditbtn.setVisibility(View.VISIBLE);
            saveupdates.setVisibility(View.GONE);
            saveupdates.setVisibility(View.INVISIBLE);
            layoutFirstname.setVisibility(View.GONE);
            layoutlastname.setVisibility(View.GONE);

            userProfileimage.setEnabled(isEditMode);
            userfirstname.setEnabled(isEditMode);
            userlastname.setEnabled(isEditMode);
            userEmail.setEnabled(isEditMode);
            userPhone.setEnabled(isEditMode);
            userabout.setEnabled(isEditMode);
            }
        userProfileimage.setEnabled(false);
        username.setEnabled(false);
        userEmail.setEnabled(false);
        userabout.setEnabled(false);
        userPhone.setEnabled(false);


    }
    private void callImagePicker() {
        try {
            if (ActivityCompat.checkSelfPermission(UserProfileviewActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(UserProfileviewActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE);
            } else {
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                openGalleryIntent.setType("image/*");
                startActivityForResult(openGalleryIntent, PICK_IMAGE);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            UiUtils.showShortToast(this, getString(R.string.sorry_no_apps_to_perform_image_picking));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            fileToUpload = data.getData();
            Glide.with(getApplicationContext())
                    .load(fileToUpload)
                    .into(userProfileimage);
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private RequestBody getPartFor(String stuff) {
        return RequestBody.create(MediaType.parse("text/plain"), stuff);
    }

}
