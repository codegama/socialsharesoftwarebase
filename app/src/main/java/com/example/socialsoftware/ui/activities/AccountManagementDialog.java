package com.example.socialsoftware.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.socialsoftware.R;
import com.example.socialsoftware.model.PageAccounts;
import com.example.socialsoftware.model.PagesManagement;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.adapter.recyclerviewadapter.PageManagementAdapter;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.NetworkUtils.*;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.utils.UiUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.internal.NavigationMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountManagementDialog extends DialogFragment{
    Unbinder unbinder;
    @BindView(R.id.pagesRecyclserView)
    RecyclerView pagesRecyclerView;
    @BindView(R.id.pageAccountback)
    ImageView pageAccountAback;
    @BindView(R.id.connectAccounts)
    TextView connectAccounts;
    @BindView(R.id.nothingHere)
    TextView nothingHere;
    PageManagementAdapter pageManagementAdapter;
    ArrayList<PagesManagement> pagesManagement=new ArrayList<>();
    APIInterface apiInterface;
    PrefUtils prefUtils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setTitle(getString(R.string.manageaccounts));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //UiUtils.showLoadingDialog(getActivity());

        return dialog;
    }
    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.alpha(0.5dp)));
            dialog.getWindow().setGravity(Gravity.START);
            setRecyclerAdapter();
        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.page_account, container, false);
        unbinder= ButterKnife.bind(this,view);
        apiInterface= APIClient.getClient().create(APIInterface.class);
        final Dialog dialog = getDialog();
        nothingHere.setVisibility(View.VISIBLE);
        //setRecyclerAdapter();
        return view;

    }

    private void setRecyclerAdapter() {
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        pageManagementAdapter=new PageManagementAdapter(getContext(), pagesManagement);
        pagesRecyclerView.setAdapter(pageManagementAdapter);
        pagesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        pagesRecyclerView.setHasFixedSize(true);
        addpages();

    }

    private void addpages() {
        prefUtils= PrefUtils.getInstance(getActivity());
        UiUtils.showLoadingDialog(getActivity());
        Call<String> call = apiInterface.listUserAccount(
                prefUtils.getIntValue(PrefKeys.USER_ID,0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    JSONObject accountsListsResponse =null;
                    try{
                        accountsListsResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if (accountsListsResponse!=null){
                        if (accountsListsResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                            UiUtils.hideLoadingDialog();
                            JSONArray dataArray = accountsListsResponse.optJSONArray(Params.DATA);
                            if (dataArray.length()!=0){
                                nothingHere.setVisibility(View.INVISIBLE);
                                pagesManagement.clear();
                                for(int i =0;i<dataArray.length();i++){

                                    try{
                                        JSONObject dataList= dataArray.getJSONObject(i);
                                        pagesManagement.add(new PagesManagement(dataList.getInt(Params.USERACCOUNT_ID)
                                                ,dataList.getInt(Params.USER_ID)
                                                ,dataList.getString(Params.PAGE_CATEGORY)
                                                ,dataList.getInt(Params.PAGE_ID)
                                                ,dataList.getString(Params.PAGE_NAME)
                                                ,dataList.getString(Params.PLATFORM)
                                                ,dataList.getString(Params.PAGE_ACCESS_TOKEN)
                                                ,dataList.getString(Params.USER_ACCESS_TOKEN)
                                                ,dataList.getString(Params.PICTURE),""));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                                pageManagementAdapter.notifyDataSetChanged();
                            } else {
                                nothingHere.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            UiUtils.showShortToast(getActivity(),accountsListsResponse.optString(Params.ERROR));
                            UiUtils.hideLoadingDialog();}
                    }
                }

            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                NetworkUtils.onApiError(getActivity());

            }
        });
    }
    @OnClick({R.id.connectAccounts,R.id.pageAccountback})
    public void onViewClicked(View view){
        switch(view.getId()){
            case R.id.connectAccounts:
                Intent addaccounts=new Intent(getActivity(),SocialButtonActivity.class);
                startActivity(addaccounts);
                break;
            case R.id.pageAccountback:
                onBackPressed();
                break;

        }
    }

    private void onBackPressed() {
        dismiss();
    }
    @Override
    public void onViewCreated(View view , @Nullable Bundle savedInstanceState){

    }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();
        pageManagementAdapter.notifyDataSetChanged();
    }

}
