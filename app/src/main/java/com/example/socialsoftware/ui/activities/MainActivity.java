package com.example.socialsoftware.ui.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.model.PageAccounts;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIEvent;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.adapter.recyclerviewadapter.PagesAccountsAdapter;
import com.example.socialsoftware.ui.fragment.ContentFragment;
import com.example.socialsoftware.ui.fragment.SettingFragment;

import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.socialsoftware.utils.Fragments.ALL_FRAGMENTS;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener{


    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.maintoolbar)
    View maintoolbar;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.bottomnavigation)
    AHBottomNavigation bottomNavigation;
    @BindView(R.id.account_container)
    FrameLayout acconutcontainer;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.toolbarUserImage)
    CircleImageView toolbarUserimage;
    @BindView(R.id.toolbarUserName)
    TextView toolbarUserName;
    TextView nothinghere;
    ActionBarDrawerToggle toggle;
    TextView navusername;
    CircleImageView navuserProfileImage;

    public  Fragment fragment;
    private ArrayList<PageAccounts> pageAccountsArrayList=new ArrayList<>();

    public static final String FIRST_TIME = "firstTime";
    public static final String ISSOCIALACCOUNTS = "issocialconnected";
    public static String CURRENT_FRAGMENT;
    APIInterface apiInterface;
    private boolean doubleTaptoExit;
    PrefUtils prefUtils;
    RecyclerView navPagesaccounts;
    private int issocialconnected;
    private boolean connected;
    PagesAccountsAdapter pagesAccountsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        apiInterface= APIClient.getClient().create(APIInterface.class);
        prefUtils=PrefUtils.getInstance(this);

        toolbarUserName.setText(prefUtils.getStringValue(PrefKeys.USER_NAME,""));
        Glide.with(this).load(prefUtils.getStringValue(PrefKeys.PICTURE,"")).into(toolbarUserimage);

        getNavigationView();
        nothinghere.setVisibility(View.VISIBLE);

        navigationView.setNavigationItemSelectedListener(this);
        //Set the BottomNavigation
        setUpBottomNavigation();
        replaceFragmentWithAnimation(new ContentFragment(),ALL_FRAGMENTS[0],false);

    }

    private void getNavigationView() {
        View headerView= navigationView.getHeaderView(0);
        navPagesaccounts = headerView.findViewById(R.id.navPagesaccounts);
        nothinghere =headerView.findViewById(R.id.nothingishere);
        nothinghere.setVisibility(View.VISIBLE);
        pagesAccountsAdapter=new PagesAccountsAdapter(getApplicationContext(),pageAccountsArrayList);
        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false);
        navPagesaccounts.setLayoutManager(linearLayoutManager);
        navPagesaccounts.setAdapter(pagesAccountsAdapter);
        navPagesaccounts.setHasFixedSize(true);
        addpagesaccount();
    }

    private void addpagesaccount() {
        Call<String> call = apiInterface.listUserAccount(
                prefUtils.getIntValue(PrefKeys.USER_ID,0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    JSONObject accountsListResponse =null;
                    try{
                        accountsListResponse = new JSONObject(response.body());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (accountsListResponse!=null){
                        if (accountsListResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                            JSONArray dataArray = accountsListResponse.optJSONArray(Params.DATA);
                            if (dataArray.length()!=0){
                                nothinghere.setVisibility(View.INVISIBLE);
                                pageAccountsArrayList.clear();
                                for(int i =0;i<dataArray.length();i++){
                                    try{
                                        JSONObject dataList= dataArray.getJSONObject(i);
                                        pageAccountsArrayList.add(new PageAccounts(dataList.getInt(Params.USERACCOUNT_ID)
                                                ,dataList.getInt(Params.USER_ID)
                                                ,dataList.getString(Params.PAGE_CATEGORY)
                                                ,dataList.getInt(Params.PAGE_ID)
                                                ,dataList.getString(Params.PAGE_NAME)
                                                ,dataList.getString(Params.PLATFORM)
                                                ,dataList.getString(Params.PAGE_ACCESS_TOKEN)
                                                ,dataList.getString(Params.USER_ACCESS_TOKEN)
                                                ,dataList.getString(Params.PICTURE),""));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                                pagesAccountsAdapter.notifyDataSetChanged();
                            }else{
                                nothinghere.setVisibility(View.VISIBLE);
                                navPagesaccounts.setVisibility(View.INVISIBLE);
                            }
                        }
                        else {
                            UiUtils.showShortToast(MainActivity.this,accountsListResponse.optString(Params.ERROR));
                        }
                    }
                } else {
                    navPagesaccounts.setVisibility(View.INVISIBLE);
                    nothinghere.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(MainActivity.this);

            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();
        registerEventBus();

        Intent caller = getIntent();
        boolean firstTime = caller.getBooleanExtra(FIRST_TIME, connected);
        Log.i("firsttime ", String.valueOf(firstTime));
        int  isConnected= caller.getIntExtra(ISSOCIALACCOUNTS,issocialconnected);
        Log.i("firsttime ", String.valueOf(isConnected));
        if (isConnected != 0 || firstTime){
            Intent mainActivity = new Intent(this, MainActivity.class);
            startActivity(mainActivity);
        }
        else {
            Intent socialActivity = new Intent(this, SocialButtonActivity.class);
            startActivity(socialActivity);
            finish();
        }

    }

    @Override
    public  void onDestroy(){
        super.onDestroy();
        unRegisterEventBus();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTokenExpriy(APIEvent event){
        unRegisterEventBus();
        logOutUserInDevice();

    }

    private void registerEventBus() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    private void unRegisterEventBus() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }



    private void replaceFragmentWithAnimation(Fragment fragment, String tag, boolean toBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        CURRENT_FRAGMENT = tag;
        this.fragment = fragment;
        if (toBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.replace(R.id.account_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void setUpBottomNavigation() {

        AHBottomNavigationItem contentfragment=new AHBottomNavigationItem(getString(R.string.content), R.drawable.ic_list_black_24dp,R.color.darkgrey);
        AHBottomNavigationItem settingfragment=new AHBottomNavigationItem(getString(R.string.setting), R.drawable.icons8_settings_24dp,R.color.darkgrey);

        bottomNavigation.addItem(contentfragment);
        bottomNavigation.addItem(settingfragment);
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setForceTint(true);
        bottomNavigation.setAccentColor(ContextCompat.getColor(this, R.color.blackblue));
        bottomNavigation.setInactiveColor(ContextCompat.getColor(this, R.color.darkgrey));
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.manageFloatingActionButtonBehavior(fab);

        bottomNavigation.setOnTabSelectedListener((position, wasSelected) -> {
            CURRENT_FRAGMENT = ALL_FRAGMENTS[position];
            switch (position){
                case 0:
                    replaceFragmentWithAnimation(new ContentFragment(), ALL_FRAGMENTS[position], false);
                    break;
                case 1:
                    replaceFragmentWithAnimation(new SettingFragment(),ALL_FRAGMENTS[position],false);
                    break;
            }
            return true;
        });
    }

    @OnClick({R.id.fab})
    public void onViewClick(View view){
        switch (view.getId()){
            case R.id.fab:
                startActivity(new Intent(MainActivity.this, CreatePostActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!CURRENT_FRAGMENT.equals(ALL_FRAGMENTS[0])) {
            if (getFragmentBackStackSize() > 0) {
                super.onBackPressed();
                return;
            } else {
                bottomNavigation.setCurrentItem(0);
                return;
            }
        }
        if (doubleTaptoExit){
            super.onBackPressed();
            finish();
            return;

        }
        doubleTaptoExit=true;
        new Handler().postDelayed(() -> doubleTaptoExit = false, 2000);
        drawer = findViewById(R.id.drawer_layout);
        /*if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/


    }
    private int getFragmentBackStackSize() {
        FragmentManager fm = getSupportFragmentManager();
        return fm.getBackStackEntryCount();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.getSupport) {
            // Handle the camera action
            Intent supportIntent= new Intent(this,StaticPageViewActivity.class);
            supportIntent.putExtra(StaticPageViewActivity.PAGE_TYPE,StaticPageViewActivity.PageTypes.HELP);
            startActivity(supportIntent);

        } else if (id == R.id.navsignout) {
            AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
            alertDialog.setTitle(getString(R.string.signing_out))
                    .setMessage(getString(R.string.you_want_signout_form_application_this_will_stop_the_remainder_from_the_accounts))
                    .setPositiveButton(getString(R.string.yes ), (dialogInterface, i) -> {
                        signOutfromApp();
                    })
            .setNegativeButton(getString(R.string.no), (dialogInterface,i)->{
                Toast.makeText(getApplicationContext(),getString( R.string.cancelled_the_signout),Toast.LENGTH_SHORT).show();
            }).show();


        } else if (id== R.id.navaddAccount){
            //startActivity(new Intent(MainActivity.this,SocialButtonActivity.class));
            //Dialog fragment for add and remove page
            FragmentManager fragmentManager=getSupportFragmentManager();
            DialogFragment dialogFragment= new AccountManagementDialog();
            dialogFragment.show(fragmentManager,"tag");

        }

        drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void signOutfromApp() {
        prefUtils=PrefUtils.getInstance(this);
        Call<String> call =apiInterface.logOutUser(
                prefUtils.getIntValue(PrefKeys.USER_ID,0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject signOutResponse = null;
                try{
                    signOutResponse = new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (signOutResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                    UiUtils.showShortToast(MainActivity.this, signOutResponse.optString(Params.MESSAGE));
                    logOutUserInDevice();
                }
                else {
                    UiUtils.showShortToast(MainActivity.this,signOutResponse.optString(Params.ERROR));
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(MainActivity.this);
            }
        });
    }
    private void logOutUserInDevice() {
        PrefHelper.setUserLoggedOut(this);
        Intent logagain= new Intent(this, InitialActivity.class);
        logagain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(logagain);
        this.finish();
    }

}
