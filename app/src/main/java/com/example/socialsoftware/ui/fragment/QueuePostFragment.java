package com.example.socialsoftware.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.model.InstaSentPostRV;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.adapter.recyclerviewadapter.QueueAdapter;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class QueuePostFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.queue_recyclerview)
    RecyclerView queuerecycler;
    @BindView(R.id.queuePause)
    TextView queuepause;
    @BindView(R.id.pauselayout)
    View pauselayout;
    @BindView(R.id.noqueuepost)
    TextView noqueuePost;
    @BindView(R.id.comingsoon)
    LottieAnimationView comingsoon;
    private QueueAdapter queueadapter;
    private APIInterface apiInterface;
    boolean doubletaptoExit;
    private ArrayList<InstaSentPostRV> instalistPost=new ArrayList<>();
    int currentPost_Id;

    public QueuePostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_queue_post, container, false);
        unbinder= ButterKnife.bind(this,view);
        apiInterface= APIClient.getClient().create(APIInterface.class);

        queueadapter=new QueueAdapter(getContext(), instalistPost);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        queuerecycler.setLayoutManager(linearLayoutManager);
        queuerecycler.setAdapter(queueadapter);
        queuerecycler.setHasFixedSize(true);
        //setRecentPost();

        comingsoon.playAnimation();
        return view;
    }


    /*private void setRecentPost() {
        PrefUtils prefUtils=PrefUtils.getInstance(getContext());
        Call<String> call=apiInterface.listPost(prefUtils.getIntValue(PrefKeys.USER_ID, 0)
        ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject recentPostResponse = null;
                try{
                    recentPostResponse= new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }
                if (recentPostResponse!=null){
                    if (recentPostResponse.optString(APIConstants.Params.SUCCESS).equals(APIConstants.Constants.TRUE)){
                        UiUtils.showShortToast(getContext(), recentPostResponse.optString(APIConstants.Params.MESSAGE));
                        JSONArray data=recentPostResponse.optJSONArray(APIConstants.Params.DATA);
                        instalistPost.clear();
                        if (data.length()!=0){
                            for (int i = data.length()-1; i >-1; i--){
                                try{
                                    JSONObject listOfPost = data.getJSONObject(i);
                                    Log.i("Posts", String.valueOf(listOfPost));
                                    instalistPost.add(new InstaSentPostRV(listOfPost.optInt(APIConstants.Params.POST_ID)
                                            ,listOfPost.optInt(APIConstants.Params.POST_ACCOUNT_ID)
                                            ,listOfPost.optString(APIConstants.Params.PLATFORM)
                                            ,listOfPost.optInt(APIConstants.Params.POST_GALLARIES)
                                            ,listOfPost.optString(APIConstants.Params.PICTURE)
                                            ,listOfPost.optString(APIConstants.Params.STATUS)
                                            ,listOfPost.optString(APIConstants.Params.DESCRIPTION)
                                            ,listOfPost.optString(APIConstants.Params.CREATED_AT)
                                    ));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            queueadapter.notifyDataSetChanged();
                        } else {
                            noqueuePost.setVisibility(View.VISIBLE); }
                    }else {
                        UiUtils.showShortToast(getContext(),recentPostResponse.optString(APIConstants.Params.ERROR));
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(getContext());
            }
        });
    }*/
    @OnClick({R.id.pauselayout,R.id.queuePause})
    public  void OnViewClick(View view){
        switch (view.getId()){
            case R.id.queuePause:
                pauselayout.setVisibility(View.GONE);
        }
   }

}
