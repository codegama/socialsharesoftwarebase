package com.example.socialsoftware.ui.adapter.recyclerviewadapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.model.InstaPastRemainderRV;
import com.example.socialsoftware.model.InstaSentPostRV;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.activities.SinglePostViewActivity;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.socialsoftware.network.APIConstants.Params.DATA;
import static com.example.socialsoftware.network.APIConstants.Params.SUCCESS;

public class QueueAdapter extends RecyclerView.Adapter <QueueAdapter.ViewHolder>{
    LayoutInflater layoutInflater;
    Context context;
    ArrayList<InstaSentPostRV> arrayList;
    APIInterface apiInterface;
    public  QueueAdapter(Context context,ArrayList<InstaSentPostRV> arrayList){
        this.context=context;
        this.arrayList=arrayList;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.insta_pastremainder_rv,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        apiInterface= APIClient.getClient().create(APIInterface.class);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final InstaSentPostRV queuePosted=arrayList.get(position);
        holder.postedId.setText(String.valueOf(queuePosted.getPostid()));
        holder.postTitle.setText(String.valueOf(queuePosted.getPostaccountid()));
        holder.descriptionText.setText(queuePosted.getDescription());
        holder.postToAccout.setText(queuePosted.getPlatfrom());
        Glide.with(context)
                .load(queuePosted.getPostimage())
                .into(holder.postedImage);

     /*   holder.queuePostOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog= new Dialog(context, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                dialog.setContentView(R.layout.dialog_posts_option);
                dialog.getWindow().setGravity(Gravity.BOTTOM);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.WRAP_CONTENT);

                TextView viewPost= dialog.findViewById(R.id.viewPost);
                TextView deletePost = dialog.findViewById(R.id.deletePost);

                viewPost.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent viewintent= new Intent(context, SinglePostViewActivity.class);
                        viewintent.putExtra(SinglePostViewActivity.VIEWPOST_ID,queuePosted.getPostid());
                        context.startActivity(viewintent);
                    }
                });
                deletePost.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deletePostPermenately(queuePosted.getPostid());
                    }
                });
            }
        });*/
    }

    private void deletePostPermenately(int postid) {
        PrefUtils prefUtils= PrefUtils.getInstance(context);
        Call<String> call= apiInterface.deletePost(
                prefUtils.getIntValue(PrefKeys.USER_ID,0)
                ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,"")
                ,postid);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject deletePostPermnatelyResponse = null;
                try{
                    deletePostPermnatelyResponse= new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }
                if (deletePostPermnatelyResponse != null){
                    if (deletePostPermnatelyResponse.optString(SUCCESS).equals(Constants.TRUE)){
                        UiUtils.showShortToast(context,deletePostPermnatelyResponse.optString(Params.MESSAGE));
                    }else {
                        UiUtils.showShortToast(context,deletePostPermnatelyResponse.optString(Params.ERROR));
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(context);

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.postedTitle)
        TextView postTitle;
        @BindView(R.id.queuePostOption)
        ImageView queuePostOption;
        @BindView(R.id.queue_PostedId)
        TextView postedId;
        @BindView(R.id.postedimage)
        ImageView postedImage;
        @BindView(R.id.descriptiontext)
        TextView descriptionText;
        @BindView(R.id.postToaccount)
        TextView postToAccout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
