package com.example.socialsoftware.ui.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;


import com.example.socialsoftware.R;
import com.example.socialsoftware.ui.adapter.ContentAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContentFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.contentTab_header)
    TabLayout contentTabheader;
    @BindView(R.id.contentView_container)
    ViewPager contentViewContainer;
    ContentAdapter contentAdapter;


    public ContentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_content, container, false);
        unbinder= ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contentAdapter=new ContentAdapter(getChildFragmentManager());
        contentViewContainer.setAdapter(contentAdapter);
    }
}
