package com.example.socialsoftware.ui.fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.activities.ChangepasswordActivity;
import com.example.socialsoftware.ui.activities.InitialActivity;
import com.example.socialsoftware.ui.activities.LoginActivity;
import com.example.socialsoftware.ui.activities.StaticPageViewActivity;
import com.example.socialsoftware.ui.activities.UserProfileviewActivity;
import com.example.socialsoftware.ui.adapter.SettingAdapter;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {
    Unbinder unbinder;
    /*@BindView(R.id.settingViewContainer)
    ViewPager settingViewContainer;
    @BindView(R.id.settingtabs)
    TabLayout settingtabs;*/
    @BindView(R.id.userProfileImage)
    CircleImageView userProfileImage;
    @BindView(R.id.userProfileName)
    TextView userProfileName;
    @BindView(R.id.userProfileEmail)
    TextView userProfileEmail;
    @BindView(R.id.profileView)
    Button profileViewbtn;
    @BindView(R.id.profileEdit)
    Button profileEditbtn;
    @BindView(R.id.changePassword)
    TextView changePassword;
    @BindView(R.id.accountSignOut)
    TextView accountSignout;
    @BindView(R.id.removeAccount)
    TextView removeAccount;
    @BindView(R.id.getAbout)
    TextView getAbout;
    @BindView(R.id.getHelp)
    TextView getHelp;
    @BindView(R.id.getPrivacyPolicy)
    TextView  getPrivacyPolicy;
    @BindView(R.id.getTermsOfUse)
    TextView getTermsofuse;

    APIInterface apiInterface;
    PrefUtils prefUtils;
    EditText userPassword;

    SettingAdapter settingAdapter;


    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder=ButterKnife.bind(this,view);
        apiInterface= APIClient.getClient().create(APIInterface.class);
        prefUtils=PrefUtils.getInstance(getActivity());
        setProfiledata();
        return  view ;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //settingAdapter=new SettingAdapter(getChildFragmentManager());
        //settingViewContainer.setAdapter(settingAdapter);
    }

    private void setProfiledata() {
        Glide.with(this).load(prefUtils.getStringValue(PrefKeys.USER_PICTURE,"")).into(userProfileImage);
        userProfileName.setText(prefUtils.getStringValue(PrefKeys.USER_NAME,""));
        userProfileEmail.setText(prefUtils.getStringValue(PrefKeys.USER_EMAIL,""));
    }

    @OnClick({R.id.profileView,R.id.profileEdit,R.id.changePassword,R.id.accountSignOut,R.id.removeAccount,R.id.getAbout,R.id.getTermsOfUse,R.id.getPrivacyPolicy,R.id.getHelp})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.profileView:
                startActivity(new Intent(getContext(), UserProfileviewActivity.class));
                break;
            case R.id.changePassword:
                Intent i=new Intent(getContext(), ChangepasswordActivity.class);
                startActivity(i);
                break;
            case R.id.accountSignOut:
                signoutfromAccount();
                break;
            case R.id.removeAccount:
                Dialog accountremovedialog= new Dialog(getContext(), WindowManager.LayoutParams.FLAG_FULLSCREEN);
                accountremovedialog.setContentView(R.layout.dialog_removeaccount);
                accountremovedialog.getWindow().setGravity(Gravity.CENTER);
                accountremovedialog.setCancelable(true);
                accountremovedialog.getWindow().setLayout(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.WRAP_CONTENT);
                userPassword= accountremovedialog.findViewById(R.id.delete_userpassword);
                TextView yes=accountremovedialog.findViewById(R.id.yes_deleteaccount);
                TextView no =accountremovedialog.findViewById(R.id.no_deleteaccount);
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (emailnotempty()) {
                            deleteAccount(userPassword.getText().toString().trim());
                        }

                    }
                });
                no.setOnClickListener(view1 -> {
                    Toast.makeText(getContext(), getString(R.string.remove_account_canceled), Toast.LENGTH_SHORT).show();
                    accountremovedialog.dismiss();
                });
                accountremovedialog.show();
                break;
            case R.id.getAbout:
                Intent aboutIntent= new Intent(getContext(), StaticPageViewActivity.class);
                aboutIntent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.ABOUT);
                startActivity(aboutIntent);
                break;
            case R.id.getHelp:
                Intent helpintent= new Intent(getContext(), StaticPageViewActivity.class);
                helpintent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.HELP);
                startActivity(helpintent);
                break;
            case R.id.getPrivacyPolicy:
                Intent privacyintent= new Intent(getContext(), StaticPageViewActivity.class);
                privacyintent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.PRIVACY);
                startActivity(privacyintent);
                break;
            case R.id.getTermsOfUse:
                Intent termsintent= new Intent(getContext(), StaticPageViewActivity.class);
                termsintent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.TERMS);
                startActivity(termsintent);
                break;
        }
    }

    private boolean emailnotempty() {
        if (userPassword.getText().toString().length()==0){
            UiUtils.showShortToast(getContext(),getString(R.string.password_cant_be_empty));
            return  false;
        }
        return true;
    }

    private void signoutfromAccount() {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.signing_out))
                .setMessage(getString(R.string.you_want_signout_form_application_this_will_stop_the_remainder_from_the_accounts))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes ), (dialogInterface, i) -> {
                    signoutUserfromDevice();
                })
                .setNegativeButton(getString(R.string.no), (dialogInterface,i)->{
                    Toast.makeText(getContext(),getString( R.string.cancelled_the_signout),Toast.LENGTH_SHORT).show();
                }).show();
    }

    private void signoutUserfromDevice() {
        Call<String> call = apiInterface.logOutUser(
                prefUtils.getIntValue(PrefKeys.USER_ID,0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    JSONObject logoutUserResponse = null;
                    try{
                        logoutUserResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if (logoutUserResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                        UiUtils.showShortToast(getContext(), logoutUserResponse.optString(Params.MESSAGE));
                        logoutUserfromDevice();
                    }
                    else {
                        UiUtils.showShortToast(getContext(),logoutUserResponse.optString( Params.ERROR));
                    }
                }
                else {
                    UiUtils.showShortToast(getContext(),Params.ERROR_MESSAGE);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(getContext());
            }
        });

    }

    private void logoutUserfromDevice() {
        PrefHelper.setUserLoggedOut(getContext());
        Intent logagain= new Intent(getContext(), LoginActivity.class);
        logagain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(logagain);
        getActivity().finish();
    }

    private void deleteAccount(String userpassword) {
        Call<String> call= apiInterface.deleteuseraccount(
                prefUtils.getIntValue(PrefKeys.USER_ID, 0)
                ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""),
                userpassword);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject deleteAccountResponse=null;
                try{
                    deleteAccountResponse= new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                if (response.isSuccessful()){
                    if (deleteAccountResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                        UiUtils.showShortToast(getContext(), deleteAccountResponse.optString(Params.MESSAGE));
                        JSONObject data= deleteAccountResponse.optJSONObject(Params.DATA);
                        Log.d("response",response.body());
                        Intent intent=new Intent(getContext(), InitialActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        //getActivity();
                    }
                    else{
                        UiUtils.showShortToast(getContext(), deleteAccountResponse.optString(Params.ERROR));
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(getContext());

            }
        });
    }

}
