package com.example.socialsoftware.ui.activities;


import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.UiUtils;
import com.example.socialsoftware.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.loginbackbtn)
    ImageButton loginbackbtn;
    @BindView(R.id.loginforgotpassword)
    TextView laoginforgotpassword;
    @BindView(R.id.layoutsignup)
    TextView layoutsignup;
    @BindView(R.id.loginbtn)
    Button loginbtn;
    @BindView(R.id.loginemail)
    TextView loginemail;
    @BindView(R.id.loginpassword)
    TextView loginpassword;


    APIInterface apiInterface;
    private  int socialaccounts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        getSupportActionBar().hide();

    }

    @OnClick({R.id.layoutsignup,R.id.loginbackbtn,R.id.loginforgotpassword,R.id.loginbtn})
    public void OnViewClicked(View view){
        switch (view.getId()){
            case R.id.layoutsignup:
                startActivity(new Intent(this,SignupActivity.class));
                finish();
                break;
            case R.id.loginbackbtn:
                onBackPressed();
                break;
            case R.id.loginforgotpassword:
                startActivity(new Intent(this,ForgotpasswordActivity.class));
                break;
            case R.id.loginbtn:
                if (validatefield()){
                    loginasuser();
                }
                break;
        }
    }

    private boolean validatefield() {
       if (loginemail.getText().toString().equals("")){
            UiUtils.showShortToast(this,getString(R.string.email_cant_be_empty));
            return  false; }
        if (!Utils.isValidEmail(loginemail.getText().toString())){
            UiUtils.showShortToast(this,getString(R.string.please_enter_valid_email));
            return false; }
       if (loginpassword.getText().toString().trim().length()==0){
            UiUtils.showShortToast(this,getString(R.string.password_cant_be_empty));
            return  false; }
        if(loginpassword.getText().toString().length() < 6){
            UiUtils.showShortToast(this,getString(R.string.password_cannot_be_less_than_six_character));
            return  false; }
        return true;
    }

    protected void loginasuser() {
        UiUtils.showLoadingDialog(this);
        Call<String> call = apiInterface.loginUser(loginemail.getText().toString()
                , loginpassword.getText().toString()
                , Constants.MANUAL_LOGIN
                , Constants.ANDROID
                , NetworkUtils.getDeviceToken(this));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                UiUtils.hideLoadingDialog();
                if (response.isSuccessful()){
                    JSONObject loginResponse = null;
                    try {
                        loginResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (loginResponse != null) {
                        if (loginResponse.optString(Params.SUCCESS).equals(Constants.TRUE)) {
                            UiUtils.showShortToast(LoginActivity.this, loginResponse.optString(Params.MESSAGE));
                            JSONObject data=loginResponse.optJSONObject(Params.DATA);
                            loginUserInDevice(data, Constants.MANUAL_LOGIN);
                        } else {
                            UiUtils.showShortToast(LoginActivity.this, loginResponse.optString(Params.ERROR));
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                NetworkUtils.onApiError(LoginActivity.this);
            }
        });
    }

    private void loginUserInDevice(JSONObject data, String loginBy) {
        PrefHelper.setUserLoggedIn(this, data.optInt(Params.USER_ID)
                , data.optString(Params.TOKEN)
                , loginBy
                , data.optString(Params.EMAIL)
                , data.optString(Params.NAME)
                , data.optString(Params.MOBILE)
                , data.optString(Params.DESCRIPTION)
                , data.optString(Params.PICTURE)
                , data.optString(Params.NOTIF_PUSH_STATUS)
                , data.optString(Params.NOTIF_EMAIL_STATUS)
                ,data.optInt(Params.IS_SOCIAL_ACCOUNT_CONNECTED));
        socialaccounts = data.optInt(Params.IS_SOCIAL_ACCOUNT_CONNECTED);
        Log.i("connected", String.valueOf(socialaccounts));
        if (data.optInt(Params.IS_SOCIAL_ACCOUNT_CONNECTED) == 0){
            Intent mainActivity = new Intent(this, SocialButtonActivity.class);
            mainActivity.putExtra(SocialButtonActivity.FIRSTTIME,  true);
            mainActivity.putExtra(SocialButtonActivity.SHOW_MORE,true);
            startActivity(mainActivity);
            this.finish();
        }else {
            Intent mainActivity = new Intent(this, MainActivity.class);
            mainActivity.putExtra(MainActivity.ISSOCIALACCOUNTS,data.optInt(Params.IS_SOCIAL_ACCOUNT_CONNECTED));
            mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainActivity);
            this.finish();
        }
    }
}

