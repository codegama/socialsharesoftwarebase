package com.example.socialsoftware.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.example.socialsoftware.R;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;

public class SplashActivity extends BaseActivity {

    private static final long SPLASH_TIME_MILLIS = 2500;
    Handler splashHandler = new Handler();

    Runnable splashRunnable = () -> {
        if (PrefUtils.getInstance(this).getBoolanValue(PrefKeys.IS_LOGGED_IN, false)) {
            Intent splashIntent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(splashIntent);
        } else {
            Intent splashIntent = new Intent(SplashActivity.this, InitialActivity.class);
            startActivity(splashIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
    }
    @Override
    protected void onStop() {
        super.onStop();
        splashHandler.removeCallbacks(splashRunnable);
    }
    @Override
    protected void onStart() {
        super.onStart();
        splashHandler.postDelayed(splashRunnable, SPLASH_TIME_MILLIS);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }
}
