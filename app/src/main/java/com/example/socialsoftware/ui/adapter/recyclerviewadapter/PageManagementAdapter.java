package com.example.socialsoftware.ui.adapter.recyclerviewadapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.socialsoftware.R;
import com.example.socialsoftware.model.PagesManagement;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.GlideApp;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PageManagementAdapter extends RecyclerView.Adapter<PageManagementAdapter.ViewHolder> {

    LayoutInflater layoutInflater;
    Context context;
    ArrayList<PagesManagement> arrayList;
    APIInterface apiInterface;
    private String seletedPages;
    private  Dialog dialog;

    public PageManagementAdapter(Context context,ArrayList<PagesManagement> arrayList){
        this.context=context;
        this.arrayList=arrayList;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    PageManagementAdapter(Dialog dialog){
        this.dialog = dialog;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.pages_accountlist,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final PagesManagement pagesManagement= arrayList.get(position);
        holder.pageName.setText(pagesManagement.getPagesname());
        holder.pageAccount.setText(pagesManagement.getPagesTypeplatform());
        GlideApp.with(context).load(pagesManagement.getPagesimage()).into(holder.pageImage);
       // GlideApp.with(context).load(pagesManagement.getPageImage()).into(holder.pageSTypePlatform);


        holder.pageAccountCard.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                //holder.pageAccountCard.setCardBackgroundColor(R.color.grey);
                Dialog pageAccountRemoveDialog= new Dialog(context, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                pageAccountRemoveDialog.setContentView(R.layout.dialog_pageremove);
                pageAccountRemoveDialog.getWindow().setGravity(Gravity.BOTTOM);
                pageAccountRemoveDialog.setCancelable(true);
                pageAccountRemoveDialog.getWindow().setLayout(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.WRAP_CONTENT);

                TextView seletedPage=pageAccountRemoveDialog.findViewById(R.id.selectedpage);
                TextView deletePage =pageAccountRemoveDialog.findViewById(R.id.deletethePage);
                ImageView cancelbtn = pageAccountRemoveDialog.findViewById(R.id.dialogcancel);

                seletedPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, "Selected this Page", Toast.LENGTH_SHORT).show();
                        //APIConstants.Params.USERACCOUNT_ID = pagesManagement.getUseraccountid();
                        PrefHelper.removeUserAccountId(context);
                        PrefHelper.setUserAccountId(context,pagesManagement.getUseraccountid());
                        Log.i("Selected Page id", String.valueOf(pagesManagement.getUseraccountid()));
                        pageAccountRemoveDialog.dismiss();
                    }
                });

                deletePage.setOnClickListener(view1 -> {
                    int pageId = pagesManagement.getUseraccountid();
                    deleteThePageaccounts(pageId);
                    pageAccountRemoveDialog.dismiss();
                    notifyDataSetChanged();

                });
                cancelbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pageAccountRemoveDialog.dismiss();
                    }
                });
                pageAccountRemoveDialog.show();
            }
        });
    }

    private void deleteThePageaccounts(int pageId) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        PrefUtils prefUtils = PrefUtils.getInstance(context);
        Call<String> call =apiInterface.deleteUserAccount(
                prefUtils.getIntValue(PrefKeys.USER_ID,0)
                ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,"")
                ,pageId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject deleteAccountResponse = null;
                try{
                    deleteAccountResponse=new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (deleteAccountResponse!=null){
                    if (deleteAccountResponse.optString(APIConstants.Params.SUCCESS).equals(APIConstants.Constants.TRUE)){
                        UiUtils.showShortToast(context,deleteAccountResponse.optString(APIConstants.Params.MESSAGE));
                        notifyDataSetChanged();
                        dialog.dismiss();
                    } else {
                        UiUtils.showShortToast(context,deleteAccountResponse.optString(APIConstants.Params.ERROR));
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(context);
            }
        });
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pageAccountCard)
        CardView pageAccountCard;
        @BindView(R.id.pageImage)
        CircleImageView pageImage;
        @BindView(R.id.pageTypePlatform)
        CircleImageView pageSTypePlatform;
        @BindView(R.id.pageName)
        TextView pageName;
        @BindView(R.id.pageAccount)
        TextView pageAccount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
