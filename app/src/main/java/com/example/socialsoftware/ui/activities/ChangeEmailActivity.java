package com.example.socialsoftware.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.socialsoftware.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangeEmailActivity extends BaseActivity {
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.save)
    Button save;
    @BindView(R.id.emailchangetoolbar)
    Toolbar  emailtoolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);
        ButterKnife.bind(this);

        getSupportActionBar().hide();
        emailtoolbar.setTitle(R.string.change_email);
        emailtoolbar.setClickable(true);

        emailtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

    }
    @OnClick({R.id.save})
    public  void OnViewClicked(View view){
        switch (view.getId()){
            case R.id.save:
                changeemail();
                break;
        }
        return;
    }

    private void changeemail() {
        if(email.getText().toString().equals("")){
            Toast.makeText(this, getString(R.string.please_enter_the_email),Toast.LENGTH_SHORT).show();
        }
    }
}
