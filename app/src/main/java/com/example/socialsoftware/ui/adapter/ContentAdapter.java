package com.example.socialsoftware.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.socialsoftware.ui.fragment.QueuePostFragment;
import com.example.socialsoftware.ui.fragment.SentPostFragment;

public class ContentAdapter extends FragmentStatePagerAdapter {
    public ContentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        if (position==0){
            fragment= new QueuePostFragment();
        }
        /*else if(position==1){
            fragment=new PastReminderFragment();
        }*/
        else if(position==1){
            fragment=new SentPostFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
    public CharSequence getPageTitle(int position){
        String title=null;
        if(position==0){
            title="Queue";
        }
        else if(position==1){
            title="Sent post";
        }
        return title;
    }

}
