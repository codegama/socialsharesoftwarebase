package com.example.socialsoftware.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaticPageViewActivity extends BaseActivity {
    @BindView(R.id.webViewToolbar)
    Toolbar webViewToolBar;
    @BindView(R.id.webstaticpages)
    ScrollView webstaticPages;
    @BindView(R.id.staticText)
    TextView staticTypeText;


    public static final String PAGE_TYPE = "pageType";
    APIInterface apiInterface;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staticpageview);
        ButterKnife.bind(this);

        getSupportActionBar().hide();
        webViewToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        Intent caller = getIntent();
        int pageType = caller.getIntExtra(PAGE_TYPE, PageTypes.ABOUT);

        apiInterface= APIClient.getClient().create(APIInterface.class);
        setRequriedStatisPage(pageType);
    }
    private void setRequriedStatisPage(int pageType) {
        switch (pageType){
            case PageTypes.ABOUT:
                webViewToolBar.setTitle("ABOUT");
                getStaticPage(APIConstants.Params.ABOUT);
                webstaticPages.setVisibility(View.VISIBLE);
                break;
            case PageTypes.PRIVACY:
                webViewToolBar.setTitle("PRIVACY");
                getStaticPage(APIConstants.Params.PRIVACY_POLICY);
                webstaticPages.setVisibility(View.VISIBLE);
                break;
            case PageTypes.TERMS:
                webViewToolBar.setTitle("TERMS");
                getStaticPage(APIConstants.Params.TERMS);
                webstaticPages.setVisibility(View.VISIBLE);
                break;
            case PageTypes.HELP:
                webViewToolBar.setTitle("HELP");
                getStaticPage(APIConstants.Params.HELP);
                webstaticPages.setVisibility(View.VISIBLE);
                break;
            case  PageTypes.SUPPORT:
                webViewToolBar.setTitle("SUPPORT");
                getStaticPage(APIConstants.Params.SUPPORT);
                webstaticPages.setVisibility(View.VISIBLE);
                break;
        }
    }

    public static final class PageTypes {
        public static final int ABOUT = 1;
        public static final int SUPPORT = 2;
        public static final int PRIVACY = 3;
        public static final int TERMS = 4;
        public static final int HELP = 5;
    }

    protected  void getStaticPage(String pageType){
        UiUtils.showLoadingDialog(this);
        Call<String> call = apiInterface.getStaticPage(pageType);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject staticPageResponse = null;
                try{
                    staticPageResponse = new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
                try{
                    if (staticPageResponse != null){
                        if (staticPageResponse.optString(APIConstants.Params.SUCCESS).equals(APIConstants.Constants.TRUE)){
                            UiUtils.hideLoadingDialog();
                            JSONObject data= staticPageResponse.optJSONObject(APIConstants.Params.DATA);
                            Log.i("Tag", String.valueOf(data));
                            staticTypeText.setText((data.optString(APIConstants.Params.DESCRIPTION)));
                        }else {
                            UiUtils.showShortToast(StaticPageViewActivity.this,staticPageResponse.optString(APIConstants.Params.ERROR));
                        }
                    }else {
                        UiUtils.hideLoadingDialog();
                        UiUtils.showShortToast(StaticPageViewActivity.this,staticPageResponse.optString(APIConstants.Params.ERROR));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                NetworkUtils.onApiError(StaticPageViewActivity.this);
            }
        });
    }
}
