package com.example.socialsoftware.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.socialsoftware.ui.fragment.AccountSettingFragment;
import com.example.socialsoftware.ui.fragment.PostSettingFragment;

public class SettingAdapter extends FragmentStatePagerAdapter {

    public SettingAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        if (position == 0){
            fragment = new PostSettingFragment();
        }
        else if(position ==1){
            fragment= new AccountSettingFragment();
        }
        return  fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
    public CharSequence getPageTitle(int position){
        String title=null;
        if (position==0){
            title="Post Setting";
        }
        else if (position==1){
            title="Account Setting";
        }
        return  title;
    }
}
