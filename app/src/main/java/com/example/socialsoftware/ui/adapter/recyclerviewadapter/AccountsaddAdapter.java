package com.example.socialsoftware.ui.adapter.recyclerviewadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.socialsoftware.R;
import com.example.socialsoftware.model.AccountsProfileImage;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AccountsaddAdapter extends RecyclerView.Adapter<AccountsaddAdapter.ViewHolder>{

    LayoutInflater layoutInflater;
    Context context;
    private ArrayList<AccountsProfileImage> arrayList;
    public AccountsaddAdapter(Context context, ArrayList<AccountsProfileImage> arrayList){
        this.context=context;
        this.arrayList=arrayList;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= layoutInflater.inflate(R.layout.accounts_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final AccountsProfileImage accountsProfileImage= arrayList.get(position);
        holder.profileimage.setImageResource(arrayList.get(position).getProfileimage());
    }

    @Override
    public int getItemCount() {
        return (arrayList == null) ? 0:arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profileimage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profileimage=itemView.findViewById(R.id.accountsprofile_image);

        }
    }
}
