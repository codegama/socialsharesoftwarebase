package com.example.socialsoftware.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.socialsoftware.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class InitialActivity extends BaseActivity{
    @BindView(R.id.initsigninbtn)
    Button initsigninbtn;
    @BindView(R.id.initsignupbtn)
    Button initsignupbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

    }
    @OnClick({R.id.initsignupbtn,R.id.initsigninbtn})
    public  void  OnViewClicked(View view){
        switch (view.getId()){
            case  R.id.initsigninbtn:
                Intent i=new Intent(InitialActivity.this, LoginActivity.class);
                startActivity(i);
                break;
            case R.id.initsignupbtn:
                Intent intent=new Intent(InitialActivity.this,SignupActivity.class);
                startActivity(intent);
                break;

        }
    }

}
