package com.example.socialsoftware.ui.fragment;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.activities.ChangeEmailActivity;
import com.example.socialsoftware.ui.activities.ChangepasswordActivity;
import com.example.socialsoftware.ui.activities.LoginActivity;
import com.example.socialsoftware.ui.activities.StaticPageViewActivity;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountSettingFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.changepassword)
    TextView changepassword;
    @BindView(R.id.accountsignout)
    TextView accountsignout;
    @BindView(R.id.gethelp)
    TextView gethelp;
    @BindView(R.id.about)
    TextView about;
    @BindView(R.id.privacypolicy)
    TextView privacypolicy;
    @BindView(R.id.termsofuse)
    TextView termsofuse;
    @BindView(R.id.emailchange)
    View emailchange;
    @BindView(R.id.frame)
    View frame;
    FragmentManager manager;
    FragmentTransaction transaction;

    APIInterface apiInterface;


    public AccountSettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_account_setting, container, false);
        unbinder= ButterKnife.bind(this,view );
        apiInterface= APIClient.getClient().create(APIInterface.class);
        return view;
    }

    @OnClick({R.id.emailchange,R.id.changepassword,R.id.accountsignout,R.id.gethelp,R.id.privacypolicy,R.id.termsofuse,R.id.about})
    public  void OnViewClicked(View view){
        switch (view.getId()){
            case R.id.emailchange:
                startActivity(new Intent(getContext(), ChangeEmailActivity.class));
                break;
            case R.id.changepassword:
                Intent i=new Intent(getContext(),ChangepasswordActivity.class);
                startActivity(i);
                break;
            case R.id.accountsignout:
                signoutAlertdialog();
                break;
            case  R.id.gethelp:
                Intent helpintent= new Intent(getContext(), StaticPageViewActivity.class);
                helpintent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.HELP);
                startActivity(helpintent);
                break;
            case R.id.about:
                Intent aboutIntent= new Intent(getContext(), StaticPageViewActivity.class);
                aboutIntent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.ABOUT);
                startActivity(aboutIntent);
                break;
            case  R.id.privacypolicy:
                Intent privacyintent= new Intent(getContext(), StaticPageViewActivity.class);
                privacyintent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.PRIVACY);
                startActivity(privacyintent);
                break;
            case R.id.termsofuse:
                Intent termsintent= new Intent(getContext(), StaticPageViewActivity.class);
                termsintent.putExtra(StaticPageViewActivity.PAGE_TYPE, StaticPageViewActivity.PageTypes.TERMS);
                startActivity(termsintent);
                break;
        }
    }

    private void signoutAlertdialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.signing_out))
                .setMessage(getString(R.string.you_want_signout_form_application_this_will_stop_the_remainder_from_the_accounts))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes ), (dialogInterface, i) -> {
                    signoutUserInDevice();
                })
                .setNegativeButton(getString(R.string.no), (dialogInterface,i)->{
                    Toast.makeText(getContext(),getString( R.string.cancelled_the_signout),Toast.LENGTH_SHORT).show();
                }).show();
    }

    private void signoutUserInDevice() {
        PrefUtils prefUtils = PrefUtils.getInstance(getContext());
        Call<String> call = apiInterface.logOutUser(
                prefUtils.getIntValue(PrefKeys.USER_ID,0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    JSONObject logoutUserResponse = null;
                    try{
                        logoutUserResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if (logoutUserResponse.optString(APIConstants.Params.SUCCESS).equals(APIConstants.Constants.TRUE)){
                        UiUtils.showShortToast(getContext(), logoutUserResponse.optString(APIConstants.Params.MESSAGE));
                        logOutUserInDevice();
                    }
                    else {
                        UiUtils.showShortToast(getContext(), logoutUserResponse.optString(APIConstants.Params.ERROR));
                    }
                }
                else {
                    UiUtils.showShortToast(getContext(),response.message());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(getContext());
            }
        });
    }

    private void changecontainer(Fragment fragment) {
        manager = getFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void logOutUserInDevice() {
        PrefHelper.setUserLoggedOut(getContext());
        Intent logagain= new Intent(getContext(), LoginActivity.class);
        logagain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(logagain);
        getActivity().finish();
    }
}
