package com.example.socialsoftware.model;

public class AccountsProfileImage {

    int profileimage;

    public AccountsProfileImage(int profileimage) {
        this.profileimage = profileimage;
    }

    public int getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(int profileimage) {
        this.profileimage = profileimage;
    }
}
