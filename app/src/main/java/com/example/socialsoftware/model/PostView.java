package com.example.socialsoftware.model;

public class PostView {
    private int postViewid,postViewUserid;
    private String postViewTilte,postviewDescription,postViewPicture,postViewAccount;

    public PostView(int postViewid, int postViewUserid, String postViewTilte, String postviewDescription, String postViewPicture, String postViewAccount) {
        this.postViewid = postViewid;
        this.postViewUserid = postViewUserid;
        this.postViewTilte = postViewTilte;
        this.postviewDescription = postviewDescription;
        this.postViewPicture = postViewPicture;
        this.postViewAccount = postViewAccount;
    }


    public int getPostViewid() {
        return postViewid;
    }

    public void setPostViewid(int postViewid) {
        this.postViewid = postViewid;
    }

    public int getPostViewUserid() {
        return postViewUserid;
    }

    public void setPostViewUserid(int postViewUserid) {
        this.postViewUserid = postViewUserid;
    }

    public String getPostViewTilte() {
        return postViewTilte;
    }

    public void setPostViewTilte(String postViewTilte) {
        this.postViewTilte = postViewTilte;
    }

    public String getPostviewDescription() {
        return postviewDescription;
    }

    public void setPostviewDescription(String postviewDescription) {
        this.postviewDescription = postviewDescription;
    }

    public String getPostViewPicture() {
        return postViewPicture;
    }

    public void setPostViewPicture(String postViewPicture) {
        this.postViewPicture = postViewPicture;
    }

    public String getPostViewAccount() {
        return postViewAccount;
    }

    public void setPostViewAccount(String postViewAccount) {
        this.postViewAccount = postViewAccount;
    }
}
